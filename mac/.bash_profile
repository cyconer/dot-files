[[ -f ~/.bashrc ]] && . ~/.bashrc

export CLICOLOR=1
export LSCOLORS=exfxbxdxcxegedabagacad
export JAVA_HOME=$(/usr/libexec/java_home)
. "$HOME/.cargo/env"
