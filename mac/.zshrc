export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="gallois"

plugins=(git vi-mode docker)

# no oh-my-zsh update
export DISABLE_AUTO_UPDATE=true

source $ZSH/oh-my-zsh.sh

export PATH="$PATH:/opt/bin:$HOME/bin"
source ~/.cargo/env
export EDITOR='vim'
export TERM=xterm-256color
#export JAVA_HOME=$(/usr/libexec/java_home)
#eval $(docker-machine env)
export LESS="$LESS --no-init --quit-if-one-screen"
export HOMEBREW_NO_AUTO_UPDATE=1

alias l1='ls -a1'
alias lt='tree -a -L 2'
alias hotcpu='pmset -g thermlog'

alias mypy='mypy --show-column-numbers --follow-imports silent'
