# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

export PATH="$PATH:$HOME/bin"

# use bash vi mode
set -o vi
export EDITOR=vim

# colored prompt
PS1='[\[\033[01;34m\]\w\[\033[00m\]]\[\033[01;32m\]\$ \[\033[00m\]'
. "$HOME/.cargo/env"
