export ZSH=$HOME/.oh-my-zsh
DISABLE_AUTO_UPDATE=true
ZSH_THEME="gallois"

# (plugins can be found in ~/.oh-my-zsh/plugins/*)
plugins=(git vi-mode docker)

DISABLE_AUTO_UPDATE=true
source $ZSH/oh-my-zsh.sh

# User configuration

export EDITOR='vim'
export TERM=xterm-256color

alias nvlc='NCURSES_ASSUMED_COLORS="-1,-1" nvlc'
alias redshift='redshift -l 52.31:13.23'
alias lt='tree -a -L 2'
alias l1='ls -1'
alias fun='firejail firefox -P fun'
alias fwork='firejail firefox -P work'
alias fchrome='firejail chromium-browser'
alias supda='sudo apt update && apt list --upgradable && sudo apt upgrade'
alias vim='vimx'
alias zathura='zathura --fork'
alias git-head='git log --oneline | head'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

function t() {
    read -k1 "answer?exit? [Y/n] "
    case "$answer" in
        y|Y|$'\n') exit;;
    esac
}
