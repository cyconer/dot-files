"""""""""""""""BASE CONFIG"""""""""""""""

" highlight the 81th column if in use
"highlight ColorColumn ctermbg=magenta
"call matchadd('ColorColumn', '\%81v', 100)

" syntax and comment hightlighting
syntax enable
syntax on
set background=dark
highlight Comment ctermfg=darkgreen
highlight Comment guifg=darkgreen
let loaded_matchparen=0     " I get confused when both brackets are highlighted

" folding, all unfolded when opening a file
set foldmethod=syntax
set foldlevelstart=99
"let g:xml_syntax_folding=1

" additional visual effects
set listchars=tab:>~,trail:␣
set list                      " use the defined listchars
"set number                    " show line numbers
set ignorecase                " ignore case when searching
set expandtab                 " expand tab to spaces
set tabstop=4                 " a tab is two spaces
set autoindent                " copy previous intendation
set shiftwidth=4              " shift by reindent >> and <<
set title                     " display title
set scrolloff=4               " min #lines above/below cursor
set laststatus=2              " always show status line
set showcmd                   " show command while typing
set wildmenu                  " visual commandline completion
"set relativenumber

" side scrolling
set nowrap
set sidescroll=1
set sidescrolloff=15

" search
set incsearch
set hlsearch

" shared clipboard
"set clipboard=unnamedplus
set clipboard^=unnamed
set clipboard^=unnamedplus


" enable scrolling with mouse, even within tmux
" no visual selection, only normal and insert mode
set mouse=ni

" netrw
autocmd FileType netrw setl bufhidden=delete    " no hidden netrw buffers that keeps vim from quitting
let g:netrw_keepdir=0         " no need to press c before copy/move
let g:netrw_banner=0          " do not show the banner


" not yet saved buffers can be hidden
set hidden

" split window to the right in vertical and below the current one in horizontal split
set splitbelow
set splitright


"""""""""""""""KEY MAPPING"""""""""""""""

" easy start of commands
" temporarily disabled to try using ; to repeat searches
" nnoremap ; :

" unindent with <S-TAB>, easier to remember and use than <C-d>
inoremap <S-TAB> <C-d>

" avoid accidental recording
nnoremap Q q
nnoremap q <Nop>

" navigate windows with space
nnoremap <Space>w <C-w>

" clear search highlighting
nnoremap <ESC><ESC> :noh<CR><ESC>

" open buffer list and load the selected buffer in the current window, in a horizontal or vertical split
nnoremap gb :ls<CR>:b<Space>
nnoremap gsb :ls<CR>:sb<Space>
nnoremap gvb :ls<CR>:vert sb<Space>

" buffer switching with tab
nnoremap <Tab> :bnext<CR>
nnoremap <S-Tab> :bprevious<CR>
nnoremap <Space>b :bnext<CR>
nnoremap <Space>B :bprevious<CR>

" F9 for make
nnoremap <F9> :make<CR>
inoremap <F9> <ESC>:w<CR>:make<CR>

if has("unix")
    if system("uname") == "Darwin\n"
        " F2 copy / F3 paste with mac clipboard
        map <F2> :.w !pbcopy<CR><CR>
        map <F3> :r !pbpaste<CR>
    endif
endif

" currency
inoremap ,euro €
inoremap ,dollar $

" german letters
inoremap ,"a ä
inoremap ,"o ö
inoremap ,"u ü
inoremap ,"A Ä
inoremap ,"O Ö
inoremap ,"U Ü
inoremap ,"s ß

" superscript and subscript
inoremap ,^0 ⁰
inoremap ,^1 ¹
inoremap ,^2 ²
inoremap ,^3 ³
inoremap ,^4 ⁴
inoremap ,^5 ⁵
inoremap ,^6 ⁶
inoremap ,^7 ⁷
inoremap ,^8 ⁸
inoremap ,^9 ⁹
inoremap ,_0 ₀
inoremap ,_1 ₁
inoremap ,_2 ₂
inoremap ,_3 ₃
inoremap ,_4 ₄
inoremap ,_5 ₅
inoremap ,_6 ₆
inoremap ,_7 ₇
inoremap ,_8 ₈
inoremap ,_9 ₉

" greek letters
inoremap ,alpha α
inoremap ,beta β
inoremap ,gamma γ
inoremap ,delta δ
inoremap ,epsilon ε
inoremap ,zeta ζ
inoremap ,eta η
inoremap ,theta θ
inoremap ,iota ι
inoremap ,kappa κ
inoremap ,lambda λ
inoremap ,mu μ
inoremap ,nu ν
inoremap ,xi ξ
inoremap ,omicron ο
inoremap ,pi π
inoremap ,rho ρ
inoremap ,sigma σ
inoremap ,varsigma ς
inoremap ,tau τ
inoremap ,upsilon υ
inoremap ,phi φ
inoremap ,chi χ
inoremap ,psi ψ
inoremap ,omega ω
inoremap ,Alpha Α
inoremap ,Beta Β
inoremap ,Gamma Γ
inoremap ,Delta Δ
inoremap ,Epsilon Ε
inoremap ,Zeta Ζ
inoremap ,Eta Η
inoremap ,Theta Θ
inoremap ,Iota Ι
inoremap ,Kappa Κ
inoremap ,Lambda Λ
inoremap ,Mu Μ
inoremap ,Nu Ν
inoremap ,Xi Ξ
inoremap ,Omicron Ο
inoremap ,Pi Π
inoremap ,Rho Ρ
inoremap ,Sigma Σ
inoremap ,Tau Τ
inoremap ,Upsilon Υ
inoremap ,Phi Φ
inoremap ,Chi Χ
inoremap ,Psi Ψ
inoremap ,Omega Ω

" operators
inoremap ,leq ≤
inoremap ,geq ≥
inoremap ,approx ≈
inoremap ,subset ⊂
inoremap ,subseteq ⊆
inoremap ,in ∊
inoremap ,cap ∩
inoremap ,cup ∪
inoremap ,forall ∀
inoremap ,exists ∃
inoremap ,partial ∂
inoremap ,nabla ∇
inoremap ,nabla^2 ∇²
inoremap ,lfloor ⌊
inoremap ,rfloor ⌋

" arrows
inoremap ,rightarrow →
inoremap ,Rightarrow ⇒
inoremap ,leftarrow ←
inoremap ,Leftarrow ⇐
inoremap ,leftrightarrow ↔
inoremap ,Leftrightarrow ⇔
inoremap ,uparrow ↑
inoremap ,downarrow ↓
inoremap ,mapsto ↦

" dots
inoremap ,ldot .
inoremap ,cdot ⋅
inoremap ,ldots …
inoremap ,cdots ⋯
inoremap ,vdots ⋮
inoremap ,ddots ⋱

" misc
inoremap ,times ×
inoremap ,norm ‖
inoremap ,tran ᵀ

" sets
inoremap ,setn ℕ
inoremap ,setz ℤ
inoremap ,setq ℚ
inoremap ,setr ℝ
inoremap ,setc ℂ
