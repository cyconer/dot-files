-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local vicious = require("vicious")

-- main screen
local mainscr = 1
if screen.count() == 3 then
    mainscr = 2
end

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(awful.util.getdir("config") .. "/themes/space/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "urxvt"
editor = os.getenv("EDITOR") or "editor"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    --awful.layout.suit.magnifier
}
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}

-- Main tags
tags1 = {
    names = {"1home", "2shell", "3file", "4chat", "5games", "6", 7, "8", "9"},
    layout = {layouts[8], layouts[4], layouts[8], layouts[2], layouts[2],
                layouts[6], layouts[6], layouts[6], layouts[6]}
}

-- Side tags of non-main monitors
tags2 = {
    names = {1,2,3,4,5,6,7,8,9},
    layout = {layouts[2], layouts[4], layouts[4], layouts[4], layouts[4],
                layouts[4], layouts[4], layouts[4], layouts[4]}
}

for s = 1, screen.count() do
    -- Each screen has its own tag table.
    if s ~= mainscr then
        tags[s] = awful.tag(tags2.names, s, tags2.layout)
    else
        tags[s] = awful.tag(tags1.names, s, tags1.layout)
    end
end
-- }}}

-- {{{ Menu
-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox

-- {{ Create a separator
separatortext = "  "
separator = wibox.widget.textbox()
--separator:set_text("  ")
separator:set_text(separatortext)

-- {{ Create a network usage widget
netwidget = wibox.widget.textbox()
vicious.register(netwidget, vicious.widgets.net,
                '<span><b>Net</b> ↘${wlp2s0 down_kb} ↗${wlp2s0 up_kb}</span>', 3)

-- {{ Create a thermal display widget
thermwidget = wibox.widget.textbox()
vicious.register(thermwidget, vicious.widgets.thermal, "<b>Temp</b> $1°C", 3, "thermal_zone0")

-- {{ Create a memory usage widget
memwidget = wibox.widget.textbox()
vicious.register(memwidget, vicious.widgets.mem, 
    function(widget, args)
        -- Determine the text with appropriate color
        function worktext(workload)
            local color = theme.fg_normal
            if workload < 70 then
                color = theme.fg_normal
            elseif workload < 90 then
                color = theme.fg_urgent
            else
                color = '#FF0000'
            end
            return '<span color="' .. color .. '"> ' .. workload .. '%</span>'
        end

        return "<b>MEM</b> " .. worktext(args[1])
    end, 13)

-- {{ Create a CPU usage widget
cpuwidget = wibox.widget.textbox()
vicious.register(cpuwidget, vicious.widgets.cpu,
    function(widget, args)
        -- Determine the text with appropriate color
        function worktext(workload)
            local color = theme.fg_normal
            if workload < 70 then
                color = theme.fg_normal
            elseif workload < 90 then
                color = theme.fg_urgent
            else
                color = '#FF0000'
            end
            return '<span color="' .. color .. '"> ' .. workload .. '%</span>'
        end
        return ('<b>CPU</b>' .. worktext(args[2]) .. worktext(args[3])
                .. worktext(args[4]) .. worktext(args[5]) )
    end, 3)

-- {{ Create a battery charge widget with plain text
batwidget = wibox.widget.textbox()
vicious.register(batwidget, vicious.widgets.bat,
    function(widget, args) 
        -- Generic display of battery charge
        function batdisplay(state, charge, color)
            return ('<span color="' .. color .. '"><b>BAT1:</b> ' ..  charge .. '%' 
                    .. state .. '</span>' .. separatortext )
        end

        -- Determine the display content
        if args[2] == 0 then
            return ""
        elseif args[2] < 8 then
            naughty.notify({
                title = '<span color="#FF0000">Warning</span>', 
                text='Battery is on ' .. args[2] .. '%.\nConnect to charger.',
                timeout = 10
            })
            return batdisplay(args[1], args[2], '#FF0000')
        elseif args[2] < 20 then
            return batdisplay(args[1], args[2], '#FF0000')
        else
            return batdisplay(args[1], args[2], theme.fg_normal)
        end
     end, 11, "BAT1")

-- {{ Create a volume display widget
volumewidget = wibox.widget.textbox()
vicious.register(volumewidget, vicious.widgets.volume,
    function(widget, args)
        --local label = { ["♫"] = "O", ["♩"] = "M" }
        --return "Volume: " .. args[1] .. "% " State: " .. label[args[2]]
        --return "♫" .. args[1] .. "%" 
        return "<b>Vol:</b> " .. args[1] .. "%" 
    end, 2, "-D pulse Master")



-- Create a textclock widget
mytextclock = awful.widget.textclock()

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({
                                                      theme = { width = 250 }
                                                  })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    if s == 1 then right_layout:add(wibox.widget.systray()) end
    --right_layout:add()
    right_layout:add(netwidget)
    right_layout:add(separator)
    right_layout:add(cpuwidget)
    right_layout:add(separator)
    right_layout:add(memwidget)
    right_layout:add(separator)
    right_layout:add(batwidget)     -- has a separator incorporated
    right_layout:add(thermwidget)
    right_layout:add(separator)
    right_layout:add(volumewidget)
    right_layout:add(separator)
    right_layout:add(mytextclock)
    right_layout:add(mylayoutbox[s])

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    -- no menu needed on right click (not existent)
    --awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Prompt
    awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end),

    -- Sound control
    awful.key({ }, "XF86AudioRaiseVolume", function()
        awful.util.spawn("amixer -D pulse set Master 5%+", false) end),
    awful.key({ }, "XF86AudioLowerVolume", function()
        awful.util.spawn("amixer -D pulse set Master 5%-", false) end),
    awful.key({ }, "XF86AudioMute", function()
        awful.util.spawn("amixer -D pulse set Master 1+ toggle", false) end),
    awful.key({ modkey, "Control" }, "Up", function()
        awful.util.spawn("amixer -D pulse set Master 5%+", false) end),
    awful.key({ modkey, "Control" }, "Down", function()
        awful.util.spawn("amixer -D pulse set Master 5%-", false) end),

    -- Change Output Device with switch_sinks script
    awful.key({ modkey, "Shift" }, "Up", function ()
        awful.util.spawn("switch_sinks") end),
    awful.key({}, "XF86AudioNext", function()
        awful.util.spawn("switch_sinks") end),

    -- Music control
    awful.key({ modkey, "Control" }, "Right", function()
        awful.util.spawn("mpc next", false) end),
    awful.key({ modkey, "Control" }, "Left", function()
        awful.util.spawn("mpc prev", false) end),
    awful.key({ modkey, "Control" }, "p", function()
        awful.util.spawn("mpc toggle", false) end),
    awful.key({}, "XF86AudioPrev", function()
        awful.util.spawn("mpc toggle", false) end),
    awful.key({ modkey, "Shift"   }, "Right", function()
        awful.util.spawn("randplay", false) end),
    awful.key({ modkey, "Control" }, "i", function()
        awful.util.spawn("mpcnotify", false) end),


    -- Mouse Click via headset
    awful.key({}, "XF86AudioPlay", function()
        awful.util.spawn("xdotool click 1") end),

    -- Monitor configuration
    -- eDP1
    awful.key({modkey},   "F5", function ()
        awful.util.spawn_with_shell("bin/monitors 1; randbg") end),
    -- DP1
    awful.key({modkey},   "F6", function ()
        awful.util.spawn_with_shell("bin/monitors 2; randbg") end),
    -- eDP1 + DP1
    awful.key({modkey},   "F7", function ()
        awful.util.spawn_with_shell("bin/monitors 12; randbg") end),
    -- HDMI2 + DP1
    awful.key({modkey},   "F8", function ()
        awful.util.spawn_with_shell("bin/monitors 23; randbg") end),
    -- HDMI2
    awful.key({modkey},   "F9", function ()
        awful.util.spawn_with_shell("bin/monitors 3; randbg") end),

    -- Move Mouse to idle position
    awful.key({ modkey, "Control" }, "m", function ()
        mouse.coords({ x=512, y=0 })
    end),

    -- Enable mouse movements with the numeric pad
    --Numeric_Pad = { "KP_End", "KP_Down", "KP_Next", "KP_Left", "KP_Begin",
    --            "KP_Right", "KP_Home", "KP_Up", "KP_Prior" }

    -- move up
    awful.key({            }, "KP_Up", function()
        awful.util.spawn("xdotool mousemove_relative --polar 0 30") end),
    -- move right
    awful.key({            }, "KP_Right", function()
        awful.util.spawn("xdotool mousemove_relative --polar 90 30") end),
    -- move down
    awful.key({            }, "KP_Down", function()
        awful.util.spawn("xdotool mousemove_relative --polar 180 30") end),
    -- move left
    awful.key({            }, "KP_Left", function()
        awful.util.spawn("xdotool mousemove_relative --polar 270 30") end),

    -- move up less
    awful.key({  "Shift"   }, "KP_Up", function()
        awful.util.spawn("xdotool mousemove_relative --polar 0 10") end),
    -- move right less
    awful.key({  "Shift"   }, "KP_Right", function()
        awful.util.spawn("xdotool mousemove_relative --polar 90 10") end),
    -- move down less
    awful.key({  "Shift"   }, "KP_Down", function()
        awful.util.spawn("xdotool mousemove_relative --polar 180 10") end),
    -- move left less
    awful.key({  "Shift"   }, "KP_Left", function()
        awful.util.spawn("xdotool mousemove_relative --polar 270 10") end),

     -- move up more
    awful.key({  "Control" }, "KP_Up", function()
        awful.util.spawn("xdotool mousemove_relative --polar 0 170") end),
    -- move right more
    awful.key({  "Control" }, "KP_Right", function()
        awful.util.spawn("xdotool mousemove_relative --polar 90 170") end),
    -- move down more
    awful.key({  "Control" }, "KP_Down", function()
        awful.util.spawn("xdotool mousemove_relative --polar 180 170") end),
    -- move left more
    awful.key({  "Control" }, "KP_Left", function()
        awful.util.spawn("xdotool mousemove_relative --polar 270 170") end),
    -- left click
    awful.key({            }, "KP_Home", function()
        awful.util.spawn("xdotool click 1") end),
    -- right click
    awful.key({            }, "KP_Prior", function()
        awful.util.spawn("xdotool click 3") end)


    -- Don't place a comma after the last element

)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end),
     awful.key({ modkey, "Shift"   }, "n",
             function()
                 local tag = awful.tag.selected()
                     for i=1, #tag:clients() do
                         tag:clients()[i].minimized=false
                         tag:clients()[i]:redraw()
                 end
             end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end),
        -- Toggle tag.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.movetotag(tag)
                          end
                     end
                  end),
        -- Toggle tag.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.toggletag(tag)
                          end
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     size_hints_honor = false,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    -- Set Firefox to always map on tags number 2 of screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { tag = tags[1][2] } },
    { rule = { class = "Evince" },
      properties = { tag = tags[mainscr][3] } },
    { rule = { class = "Zathura" },
      properties = { tag = tags[mainscr][3] } },
    { rule = { class = "Firefox" },
      properties = { tag = tags[mainscr][1] } },
    { rule = { class = "Vlc" },
      properties = { tag = tags[mainscr][1] } },
    { rule = { class = "libreoffice" },
      properties = { tag = tags[mainscr][3] } },
    { rule = { class = "LibreOffice" },
      properties = { tag = tags[mainscr][3] } },
    { rule = { class = "Thunderbird" },
      properties = { tag = tags[mainscr][4] } },
    { rule = { class = "Skype" },
      properties = { tag = tags[mainscr][4] } },
    { rule = { class = "TeamSpeak 3" },
      properties = { tag = tags[mainscr][4] } },
    { rule = { class = "Genymotion" },
      properties = { tag = tags[mainscr][4] } },
    { rule = { class = "Steam" },
      properties = { tag = tags[mainscr][5] } },
    { rule = { class = "Fceux" },
      properties = { floating = true } },
    { rule = { instance = "plugin-container" },
      properties = { floating = true } },
    { rule = { instance = "Plugin-container" },
      properties = { floating = true } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align("center")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
